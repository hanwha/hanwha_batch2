﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sales.Illustration.Web.Helper;

namespace Sales.Illustration.Web.Extensions
{
    public class StaticDropdownValues
    {
        public static List<SelectListItem> GetRelation(string type)
        {
            if (type == "traditional")
            {
                var result = new List<SelectListItem>
                {
                    new SelectListItem() {Text = "Pasangan", Value = "Pasangan"}
                };

                return result;
            }
            else
            {
                var result = new List<SelectListItem>
                {
                    new SelectListItem() {Text = "Pasangan", Value = "Pasangan"},
                    new SelectListItem() {Text = "Orang Tua Kandung", Value = "Orang Tua Kandung"},
                    new SelectListItem() {Text = "Anak Kandung", Value = "Anak Kandung"},
                    new SelectListItem() {Text = "Orang Tua Angkat", Value = "Orang Tua Angkat"},
                    new SelectListItem() {Text = "Anak Angkat", Value = "Anak Angkat"},
                    new SelectListItem() {Text = "Kakak Kandung", Value = "Kakak Kandung"},
                    new SelectListItem() {Text = "Adik Kandung", Value = "Adik Kandung"},
                    new SelectListItem() {Text = "Paman / Bibi", Value = "Paman / Bibi"},
                    new SelectListItem() {Text = "Keponakan", Value = "Keponakan"},
                    new SelectListItem() {Text = "Cucu", Value = "Cucu"},
                    new SelectListItem() {Text = "Debitur", Value = "Debitur"},
                    new SelectListItem() {Text = "Pengurus Yayasan", Value = "Pengurus Yayasan"},
                    new SelectListItem() {Text = "Karyawan", Value = "Karyawan"},
                    new SelectListItem() {Text = "Lainnya", Value = "Lainnya"}
                };
                var inpList = new SelectListItem() { Text = "Diri Sendiri", Value = "Diri Sendiri" };
                if(type == "AdditionalInsured")
                    result.Insert(1, inpList);

                return result;
            }
        }

        public static List<SelectListItem> GetPaymentMode()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() {Text = "Tunggal", Value = "sekaligus"},
                new SelectListItem() {Text = "Berkala", Value = "berkala"}
            };

            return result;
        }

        public static List<SelectListItem> GetPaymentPeriod()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() {Text = "18 - Usia Masuk Anak", Value = "18"},
                new SelectListItem() {Text = "15", Value = "15"},
                new SelectListItem() {Text = "10", Value = "10"},
                new SelectListItem() {Text = "5", Value = "5"}
            };

            return result;
        }

        public static List<SelectListItem> GetProductType(string type)
        {
            if (type == "tra")
            {
                var result = new List<SelectListItem>
            {
                new SelectListItem() {Text = "Traditional", Value = "tra"}
            };

                return result;
            }
            else
            {
                var result = new List<SelectListItem>
            {
                new SelectListItem() {Text = "Unit Link", Value = "ul"}
            };

                return result;
            }
        }

        public static List<SelectListItem> getPercentages()
        {
            var data = new List<SelectListItem>();

            for (int i = 0; i <= 100; i += 5)
            {
                data.Add(new SelectListItem() { Text = i + "", Value = i + "" });
            }
            return data;
        }

        public static List<SelectListItem> GetUnit()
        {
            var data = new List<SelectListItem>();

            for (int i = 1; i <= 4; i++)
            {
                data.Add(new SelectListItem() { Text = i + "", Value = i + "" });
            }
            return data;
        }

        public static List<SelectListItem> GetUnitWithSelected(string selected)
        {
            var data = new List<SelectListItem>();
            for (int i = 1; i <= 4; i++)
            {
                var select = (i + "") == selected ? true : false;
                data.Add(new SelectListItem() { Text = i + "", Value = i + "", Selected = select });
            }
            return data;
        }

        public static List<SelectListItem> getNinentyNineYears()
        {
            var data = new List<SelectListItem>();

            for (int i = 1; i < 100; i++)
            {
                data.Add(new SelectListItem() { Text = i + "", Value = i + "" });
            }
            return data;
        }

        public static List<SelectListItem> getNinetyNineYearsWithStart(int startYear)
        {
            var data = new List<SelectListItem>();

            for (int i = startYear; i < 100; i++)
            {
                data.Add(new SelectListItem() { Text = i + "", Value = i + "" });
            }
            return data;
        }

        public static List<SelectListItem> GetRiderCoverage()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() {Text = "Selama Masa Pembayaran Premi", Value = "1"},
                new SelectListItem() {Text = "Selama Kontrak atau maksimum Usia Tertanggung 65", Value = "2"}
            };

            return result;
        }

        public static List<SelectListItem> GetStatusMerokok()
        {
            var result = new List<SelectListItem>
            {
                new SelectListItem() {Text = "Merokok", Value = "Merokok"},
                new SelectListItem() {Text = "Tidak Merokok", Value = "Tidak Merokok"}
            };

            return result;
        }

        public static List<SelectListItem> getHundredValues()
        {
            var data = new List<SelectListItem>();

            for (int i = 0; i <= 100; i++)
            {
                data.Add(new SelectListItem() { Text = i + "", Value = i + "" });
            }
            return data;
        }

        public static List<SelectListItem> getHundredValuesWithSelected(string selected)
        {
            var data = new List<SelectListItem>();

            for (int i = 0; i <= 100; i++)
            {
                var select = (i + "") == selected ? true : false;
                data.Add(new SelectListItem() { Text = i + "", Value = i + "", Selected = select });
            }
            return data;
        }
    }
}
