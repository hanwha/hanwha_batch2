﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Helper
{
    public class Generator : Controller
    {
        //
        // GET: /Generator/

        public static string toPassword(string password)
        {
            return CustomEncryption.HashStringSHA1(password);
        }

    }
}
