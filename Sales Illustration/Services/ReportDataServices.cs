﻿using Sales.Illustration.Web.Domain;
using Sales.Illustration.Web.Models;
using Sales.Illustration.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Sales.Illustration.Web.Services
{
    public class InvestmentFundRow
    {
        public int Tahun { get; set; }
        public decimal Low { get; set; }
        public decimal Med { get; set; }
        public decimal High { get; set; }
    }

    public static class ReportDataServices
    {
        public static DataTable General(SummaryViewModel dataInput, string agentName, string agentCode, string appVersion, Product product)
        {
            var dtGeneral = new DataTable { TableName = "General" };

            dtGeneral.Columns.Add("Version");
            dtGeneral.Columns.Add("AgentCode");
            dtGeneral.Columns.Add("AgentName");
            dtGeneral.Columns.Add("HasAsuransiTambahan");
            dtGeneral.Columns.Add("BiayaAsuransi");
            dtGeneral.Columns.Add("BiayaAsuransiUtama");
            dtGeneral.Columns.Add("BiayaAsuransiTambahan");
            dtGeneral.Columns.Add("TotalBiayaAsuransi");
            dtGeneral.Columns.Add("DueDate");

            var biayaAsuransi = product.ProductType.Equals("tra") ? CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) : Math.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.BiayaAsuransi) / 12, 0);
            var biayaAsuransiUtama = product.ProductType.Equals("tra") ? CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan) : Math.Round(CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan) / 12, 0);

            decimal biayaAsuransiTambahan = 0;

            if (product.HasAdditionalInsured == true)
                biayaAsuransiTambahan = dataInput.Additional.TertanggungTambahan != null ? Math.Round(CalculatorServices.CurrencyToDecimal(dataInput.Additional.BiayaAsuransiTertanggungTambahan) / 12, 0) : 0;

            var totalBiayaAsuransi = biayaAsuransi + biayaAsuransiUtama + biayaAsuransiTambahan;

            var newRow = dtGeneral.NewRow();
            newRow["Version"] = appVersion;
            newRow["AgentCode"] = agentCode;
            newRow["AgentName"] = agentName;
            newRow["HasAsuransiTambahan"] = product.HasAdditionalInsured;
            newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(biayaAsuransi);
            newRow["BiayaAsuransiUtama"] = CalculatorServices.DecimalToCurrency(biayaAsuransiUtama);
            newRow["BiayaAsuransiTambahan"] = CalculatorServices.DecimalToCurrency(biayaAsuransiTambahan);
            newRow["TotalBiayaAsuransi"] = CalculatorServices.DecimalToCurrency(totalBiayaAsuransi);

            var dateNow = DateTime.Now.Date;
            var dueDate = dateNow.AddDays(60);

            newRow["DueDate"] = dueDate.ToString("dd-MMM-yyyy");
            dtGeneral.Rows.Add(newRow);

            return dtGeneral;
        }

        public static DataTable NasabahData(SummaryViewModel dataInput, Product product)
        {
            var dtNasabah = new DataTable { TableName = "NasabahData" };
            dtNasabah.Columns.Add("DataType");
            dtNasabah.Columns.Add("Data");

            string insName;
            string age;
            string gender;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
            {
                insName = dataInput.Nasabah.NamaPemegangPolis;
                age = dataInput.Nasabah.UmurPemegangPolis.ToString();
                gender = dataInput.Nasabah.JenisKelaminPemegangPolis;
            }
            else
            {
                insName = dataInput.Nasabah.NamaTertanggungUtama;
                age = dataInput.Nasabah.UmurTertanggungUtama.ToString();
                gender = dataInput.Nasabah.JenisKelaminTertanggungUtama;
            }

            var newRow = dtNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.NamaTertanggung;
            newRow["Data"] = insName;
            dtNasabah.Rows.Add(newRow);

            newRow = dtNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
            newRow["Data"] = age + App_Data.Text.TahunSpasi + " / " + (gender == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
            dtNasabah.Rows.Add(newRow);

            newRow = dtNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.MataUang;
            newRow["Data"] = dataInput.Premi.MataUang == "IDR" ? App_Data.Text.Rupiah : App_Data.Text.Rupiah;
            dtNasabah.Rows.Add(newRow);

            newRow = dtNasabah.NewRow();

            if (product.ProductType.Equals("tra"))
            {
                newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;

                if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
                {
                    newRow["Data"] = App_Data.Text.Sekaligus;
                }
                else
                {
                    var payTerm = dataInput.Premi.PilihanMasaPembayaran == "18" ? (Convert.ToInt32(dataInput.Premi.PilihanMasaPembayaran) - dataInput.Nasabah.UmurAnak).ToString() : dataInput.Premi.PilihanMasaPembayaran;
                    newRow["Data"] = payTerm + App_Data.Text.TahunSpasi;
                }
            }
            else
            {
                newRow["DataType"] = App_Data.Text.RencanaMasaPembayaranPremi;

                if (product.ProductCategory.Equals("single"))
                {
                    newRow["Data"] = App_Data.Text.PremiDasar;
                }
                else
                {
                    newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + App_Data.Text.TahunSpasi;
                }
            }

            dtNasabah.Rows.Add(newRow);

            return dtNasabah;
        }

        public static DataTable DataAnakNasabah(SummaryViewModel dataInput)
        {
            var dtAnakNasabah = new DataTable { TableName = "DataAnakNasabah" };
            dtAnakNasabah.Columns.Add("DataType");
            dtAnakNasabah.Columns.Add("Data");

            var newRow = dtAnakNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.NamaAnak;
            newRow["Data"] = dataInput.Nasabah.NamaAnak;
            dtAnakNasabah.Rows.Add(newRow);

            newRow = dtAnakNasabah.NewRow();
            newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
            newRow["Data"] = dataInput.Nasabah.UmurAnak + App_Data.Text.TahunSpasi + " / " + (dataInput.Nasabah.JenisKelaminAnak == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
            dtAnakNasabah.Rows.Add(newRow);

            return dtAnakNasabah;
        }

        public static DataTable PremiData(SummaryViewModel dataInput, Product product)
        {
            var dtPremi = new DataTable { TableName = "PremiData" };

            dtPremi.Columns.Add("DataType");
            dtPremi.Columns.Add("Data");

            var newRow = dtPremi.NewRow();

            string caraBayar, berkala;

            if (dataInput.Premi.CaraBayar == "1")
            {
                caraBayar = App_Data.Text.Tahunan;
                berkala = App_Data.Text.TahunSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "2")
            {
                caraBayar = App_Data.Text.Semesteran;
                berkala = App_Data.Text.SemeterSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "4")
            {
                caraBayar = App_Data.Text.Kuartalan;
                berkala = App_Data.Text.KuartalSpasi;
            }
            else
            {
                caraBayar = App_Data.Text.Bulanan;
                berkala = App_Data.Text.BulanSpasi;
            }

            if (product.ProductType.Equals("ul"))
            {
                var totalPremi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);

                if (product.ProductCategory.Equals("single"))
                {
                    newRow["DataType"] = App_Data.Text.PremiTunggal;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PilihanUangPertanggungan;
                    newRow["Data"] = Math.Round((CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan) / CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) * 100)) + "%";
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.UangPertanggungan;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.UangPertanggungan;
                    dtPremi.Rows.Add(newRow);

                    //newRow = dtPremi.NewRow();
                    //newRow["DataType"] = App_Data.Text.MasaPertanggunganInvestasi;
                    //newRow["Data"] = App_Data.Text.SampaiDengan + (product.CovAge / 12) + App_Data.Text.TahunSpasi;
                    //dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiTopUpSekaligus;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.TopupBerkala;
                    dtPremi.Rows.Add(newRow);

                    //if (dataInput.TopUp.TopupWithdrawals != null)
                    //{
                    //    var firstTopup = dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp).OrderBy(x => x.Year).FirstOrDefault();

                    //    if (firstTopup != null)
                    //    {
                    //        newRow = dtPremi.NewRow();
                    //        newRow["DataType"] = App_Data.Text.PremiTopupThnKe + firstTopup.Year;
                    //        newRow["Data"] = dataInput.StrCurrency + firstTopup.Amount;
                    //        dtPremi.Rows.Add(newRow);
                    //    }
                    //}
                }
                else
                {
                    newRow["DataType"] = App_Data.Text.PremiBerkala;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala + " /" + App_Data.Text.TahunSpasi;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.TopUpBerkala;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala)) + " /" + App_Data.Text.TahunSpasi;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.TotalPremiTahunan;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(totalPremi) + " /" + App_Data.Text.TahunSpasi;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.CaraPembayaran;
                    newRow["Data"] = caraBayar;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiAngsuran;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(totalPremi / Convert.ToInt32(dataInput.Premi.CaraBayar)) + " /" + berkala;
                    dtPremi.Rows.Add(newRow);
                }
            }
            else
            {
                newRow["DataType"] = App_Data.Text.PremiAsuransiDasar;

                if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
                {
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala ;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiAsuransiTambahan;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan ;
                    dtPremi.Rows.Add(newRow);

                    var total = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.TotalPremi;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total) ;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.CaraPembayaranPremi;
                    newRow["Data"] = App_Data.Text.Sekaligus;
                    dtPremi.Rows.Add(newRow);
                }
                else
                {
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala + " /" + berkala;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.PremiAsuransiTambahan;
                    newRow["Data"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan + " /" + berkala;
                    dtPremi.Rows.Add(newRow);

                    var total = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.TotalPremi;
                    newRow["Data"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total) + " /" + berkala;
                    dtPremi.Rows.Add(newRow);

                    newRow = dtPremi.NewRow();
                    newRow["DataType"] = App_Data.Text.CaraPembayaranPremi;
                    newRow["Data"] = caraBayar;
                    dtPremi.Rows.Add(newRow);
                }
            }

            return dtPremi;
        }

        public static DataTable FundData(SummaryViewModel dataInput)
        {
            var dtFund = new DataTable { TableName = "FundData" };
            dtFund.Columns.AddRange(new[] { new DataColumn("Name"), new DataColumn("Percentage") });

            foreach (var t in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
            {
                var fund = dtFund.NewRow();
                fund["Name"] = t.InvestmentName;
                fund["Percentage"] = t.Percentage + "%";
                dtFund.Rows.Add(fund);
            }

            return dtFund;
        }

        public static DataTable RincianAsuransiDasar(SummaryViewModel dataInput, Product product)
        {
            var dtAsuransi = new DataTable { TableName = "RincianAsuransiDasar" };

            dtAsuransi.Columns.Add("NamaProduk");
            dtAsuransi.Columns.Add("MasaPertanggungan");
            dtAsuransi.Columns.Add("UangPertanggungan");
            dtAsuransi.Columns.Add("BiayaAsuransi");

            var insAge = 0;
            var period = dataInput.Premi.MasaAsuransi;
            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            var newRow = dtAsuransi.NewRow();
            newRow["NamaProduk"] = product.ProductName;
            newRow["MasaPertanggungan"] = product.ProductType.Equals("tra") ? (period + insAge) : product.CovAge / 12;
            newRow["UangPertanggungan"] = dataInput.Premi.UangPertanggungan;

            if (product.ProductType.Equals("ul"))
                newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal(dataInput.Premi.BiayaAsuransi) / 12, 0));
            else
                newRow["BiayaAsuransi"] = dataInput.Premi.PremiBerkala;

            dtAsuransi.Rows.Add(newRow);

            return dtAsuransi;
        }

        public static DataTable RiderDesc(SummaryViewModel dataInput, List<Rider> riderList, List<Rider> riderListAddins, bool isEnglish, Product product)
        {
            var dtRider = new DataTable { TableName = "RiderDesc" };

            dtRider.Columns.Add("RiderName");
            dtRider.Columns.Add("RiderDesc");

            foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
            {
                var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                if (riderItem.Rider.Category.Equals("Choices"))
                {
                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                    {
                        foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                        {
                            var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                            var newRow = dtRider.NewRow();
                            newRow["RiderName"] = rider.RiderName + " - " + riderType.TypeName;

                            if (product.ProductType.Equals("tra"))
                            {
                                newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                            }
                            else if (product.ProductCode.Equals("HLWIZ"))
                            {
                                newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                            }
                            else
                            {
                                newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN : riderType.Description;
                            }

                            dtRider.Rows.Add(newRow);
                        }
                    }
                }
                else
                {
                    var newRow = dtRider.NewRow();
                    newRow["RiderName"] = rider.RiderName;

                    if (product.ProductType.Equals("tra"))
                    {
                        newRow["RiderDesc"] = isEnglish ? rider.DescriptionEN.Replace("70 y", "65 y") : rider.Description.Replace("70 t", "65 t");
                    }
                    else if (product.ProductCode.Equals("HLWIZ"))
                    {
                        newRow["RiderDesc"] = isEnglish ? rider.DescriptionEN.Replace("70 y", "65 y") : rider.Description.Replace("70 t", "65 t");
                    }
                    else
                    {
                        newRow["RiderDesc"] = isEnglish ? rider.DescriptionEN : rider.Description;
                    }

                    dtRider.Rows.Add(newRow);
                }
            }


            if (riderListAddins != null)
            {
                foreach (var riderItem in dataInput.Additional.Riders.Where(x => x.Checked || (x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0) || x.Rider.Category.Equals("Choices")))
                {
                    var rider = riderListAddins.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                    if (dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0 && x.Rider.RiderCode == riderItem.Rider.RiderCode).Count() <= 0)
                    {
                        if (riderItem.Rider.Category.Equals("Choices"))
                        {
                            if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                            {
                                foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                                {
                                    var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                                    var newRow = dtRider.NewRow();
                                    newRow["RiderName"] = rider.RiderName + " - " + riderType.TypeName;

                                    if (product.ProductType.Equals("tra"))
                                    {
                                        newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                                    }
                                    else if (product.ProductCode.Equals("HLWIZ"))
                                    {
                                        newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN.Replace("70 y", "65 y") : riderType.Description.Replace("70 t", "65 t");
                                    }
                                    else
                                    {
                                        newRow["RiderDesc"] = isEnglish ? riderType.DescriptionEN : riderType.Description;
                                    }

                                    dtRider.Rows.Add(newRow);
                                }
                            }
                        }
                    }
                }
            }
            return dtRider;
        }

        public static DataTable RiderWithDesc(SummaryViewModel dataInput, List<Rider> riderList)
        {
            var dtRiderWithDesc = new DataTable { TableName = "RiderWithDesc" };
            var riderAll = dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0).ToList();
            var addIns = dataInput.Additional == null ? null : dataInput.Additional.Riders == null ? null : dataInput.Additional.Riders.Where(x => x.Checked || x.Rider.Category.Equals("Choices")).ToList();

            if(addIns != null)
                riderAll.AddRange(addIns);            

            foreach (var riderItem in riderAll)
            {
                var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                {
                    foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                    {
                        var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                        if (riderType.TypeName.Equals("CI"))
                        {
                            if (dtRiderWithDesc.Rows.Count > 0)
                            {
                                dtRiderWithDesc.Rows[0][riderType.TypeName] = riderType.TypeName;
                            }
                            else
                            {
                                dtRiderWithDesc.Columns.Add("CI");
                                dtRiderWithDesc.Columns.Add("CI Accelerated");
                                dtRiderWithDesc.Columns.Add("HCP");
                                dtRiderWithDesc.Columns.Add("AHCP");
                                dtRiderWithDesc.Columns.Add("Medi Guard");
                                dtRiderWithDesc.Columns.Add("Hanwha Health Protection");

                                var row = dtRiderWithDesc.NewRow();
                                row["CI"] = "NULL";
                                row["CI Accelerated"] = "NULL";
                                row["HCP"] = "NULL";
                                row["AHCP"] = "NULL";
                                row["Medi Guard"] = "NULL";
                                row["Hanwha Health Protection"] = "NULL";
                                row[riderType.TypeName] = riderType.TypeName;
                                dtRiderWithDesc.Rows.Add(row);
                            }
                        }
                    }
                }
                else
                {
                    if (rider.RiderName.Equals("CI") || rider.RiderName.Equals("CI Accelerated") || rider.RiderName.Equals("HCP") || rider.RiderName.Equals("AHCP") || rider.RiderName.Equals("Medi Guard") || rider.RiderName.Equals("Hanwha Health Protection"))
                    {
                        var riderChoice = rider.RiderCode == "HHP" ? rider.RiderTypes.Where(x => x.RiderTypeId.ToString() == riderItem.Choice).FirstOrDefault().TypeName : rider.RiderName;
; 
                        if (dtRiderWithDesc.Rows.Count > 0)
                        {
                            dtRiderWithDesc.Rows[0][rider.RiderName] = rider.RiderCode == "HHP" ? riderChoice : rider.RiderName;
                        }
                        else
                        {
                            dtRiderWithDesc.Columns.Add("CI");
                            dtRiderWithDesc.Columns.Add("CI Accelerated");
                            dtRiderWithDesc.Columns.Add("HCP");
                            dtRiderWithDesc.Columns.Add("AHCP");
                            dtRiderWithDesc.Columns.Add("Medi Guard");
                            dtRiderWithDesc.Columns.Add("Hanwha Health Protection");

                            var row = dtRiderWithDesc.NewRow();
                            row["CI"] = "NULL";
                            row["CI Accelerated"] = "NULL";
                            row["HCP"] = "NULL";
                            row["AHCP"] = "NULL";
                            row["Medi Guard"] = "NULL";
                            row["Hanwha Health Protection"] = "NULL";
                            row[rider.RiderName] = riderChoice;
                            dtRiderWithDesc.Rows.Add(row);
                        }
                    }
                }
            }

            if (dtRiderWithDesc.Rows.Count < 1)
            {
                dtRiderWithDesc.Columns.Add("CI");
                dtRiderWithDesc.Columns.Add("CI Accelerated");
                dtRiderWithDesc.Columns.Add("HCP");
                dtRiderWithDesc.Columns.Add("AHCP");
                dtRiderWithDesc.Columns.Add("Medi Guard");
                dtRiderWithDesc.Columns.Add("Hanwha Health Protection");

                var row = dtRiderWithDesc.NewRow();
                row["CI"] = "NULL";
                row["CI Accelerated"] = "NULL";
                row["HCP"] = "NULL";
                row["AHCP"] = "NULL";
                row["Medi Guard"] = "NULL";
                row["Hanwha Health Protection"] = "NULL";
                dtRiderWithDesc.Rows.Add(row);
            }

            return dtRiderWithDesc;
        }

        public static DataTable RincianRider(SummaryViewModel dataInput, List<Rider> riderList, Product product)
        {
            var dtRider = new DataTable { TableName = "RincianRider" };

            dtRider.Columns.Add("RiderName");
            dtRider.Columns.Add("MasaPertanggungan");
            dtRider.Columns.Add("UangPertanggungan");
            dtRider.Columns.Add("BiayaAsuransi");

            foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
            {
                var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);
                
                // Traditional cov term 65 year
                if (product.ProductType.Equals("tra"))
                    rider.CoverTerm = 780;

                // Hanwha Wizer cov term 70 year
                if (product.ProductCode.Equals("HLWIZ"))
                    rider.CoverTerm = riderItem.Rider.CoverTermWizer;

                if (riderItem.Rider.Category.Equals("Choices"))
                {
                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                    {
                        foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                        {
                            var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                            var newRow = dtRider.NewRow();
                            newRow["RiderName"] = riderItem.Rider.RiderName + " - " + riderType.TypeName;
                            newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                            newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                            decimal biayaAsuransi;

                            if (product.ProductType.Equals("tra"))
                            {
                                biayaAsuransi = MainServices.CalculateTraditionalCOR(riderItem.UangPertanggungan, riderItem.Rider.RiderCode, riderType.RiderTypeId, dataInput.Premi.ModeBayarPremi, dataInput.Premi.RencanaMasaPembayaran, riderItem.Rider.Category);
                                biayaAsuransi = CalculatorServices.Round(biayaAsuransi * Convert.ToDecimal((MainServices.GetFactorPaymentMethod(Convert.ToInt32(dataInput.Premi.CaraBayar)) / 100)), -1);
                            }
                            else
                            {
                                int age = 0;

                                if (riderItem.Rider.RiderCode.Equals("POP"))
                                    age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                                else
                                {
                                    if (dataInput.Nasabah.TertanggungUtama.Equals("Ya"))
                                        age = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                                    else
                                        age = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);
                                }

                                biayaAsuransi = MainServices.CalculateUnitLinkCOR(CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan), riderItem.Rider.RiderCode, riderType.RiderTypeId, age, null, riderItem.Rider.Category, null);
                            }

                            newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                            dtRider.Rows.Add(newRow);
                        }
                    }
                }
                else if (riderItem.Rider.Category.Equals("Unit"))
                {
                    if (!String.IsNullOrEmpty(riderItem.UnitName))
                    {
                        var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.UnitName));

                        var newRow = dtRider.NewRow();
                        newRow["RiderName"] = riderItem.Unit + " X " + riderType.TypeName;
                        newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                        newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                        newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                        dtRider.Rows.Add(newRow);
                    }
                }
                else if (riderItem.Rider.Category.Equals("Choice"))
                {
                    if (!String.IsNullOrEmpty(riderItem.Choice))
                    {
                        var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.Choice));

                        var newRow = dtRider.NewRow();
                        newRow["RiderName"] = riderItem.Rider.RiderName.Replace("Hanwha","").Trim() + " - " + riderType.TypeName;
                        newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                        newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                        newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                        dtRider.Rows.Add(newRow);
                    }
                }
                else
                {
                    var newRow = dtRider.NewRow();
                    newRow["RiderName"] = riderItem.Rider.RiderName;
                    newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                    newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                    newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                    dtRider.Rows.Add(newRow);
                }
            }

            return dtRider;
        }

        public static DataTable RincianRiderTambahan(SummaryViewModel dataInput, List<Rider> riderList, Product product)
        {
            var dtRider = new DataTable { TableName = "RincianRiderTambahan" };

            dtRider.Columns.Add("RiderName");
            dtRider.Columns.Add("MasaPertanggungan");
            dtRider.Columns.Add("UangPertanggungan");
            dtRider.Columns.Add("BiayaAsuransi");

            if (dataInput.Additional.TertanggungTambahan != null)
            {
                for (var i = 0; i < dataInput.Additional.TertanggungTambahan.Count; i++)
                {
                    for (int y = i; y < dataInput.Additional.Riders.Count;)
                    {
                        var riderItem = dataInput.Additional.Riders[y];
                        var rider = riderList.FirstOrDefault(x => x.RiderCode == riderItem.Rider.RiderCode);

                        if (riderItem.Checked || CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0 || riderItem.Rider.Category.Equals("Choices"))
                        {
                            if (riderItem.Rider.Category.Equals("Choices"))
                            {
                                if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                {
                                    foreach (var riderTypeItem in riderItem.Choices.Where(x => x.Checked))
                                    {
                                        var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == riderTypeItem.RiderType.RiderTypeId);

                                        var newRow = dtRider.NewRow();
                                        newRow["RiderName"] = riderItem.Rider.RiderName + " - " + riderType.TypeName + " (TT" + (i + 1) + ")";
                                        newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                                        newRow["UangPertanggungan"] = riderItem.UangPertanggungan;

                                        decimal biayaAsuransi;

                                        if (product.ProductType.Equals("tra"))
                                        {
                                            biayaAsuransi = MainServices.CalculateTraditionalCOR(riderItem.UangPertanggungan, riderItem.Rider.RiderCode, riderType.RiderTypeId, dataInput.Premi.ModeBayarPremi, dataInput.Premi.RencanaMasaPembayaran, riderItem.Rider.Category);
                                        }
                                        else
                                        {
                                            int age = (int)dataInput.Additional.TertanggungTambahan[i].Age;

                                            biayaAsuransi = MainServices.CalculateUnitLinkCOR(CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan), riderItem.Rider.RiderCode, riderType.RiderTypeId, age, null, riderItem.Rider.Category, null);
                                        }

                                        newRow["BiayaAsuransi"] = CalculatorServices.DecimalToCurrency(biayaAsuransi);
                                        dtRider.Rows.Add(newRow);
                                    }
                                }
                            }
                            else if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                if (!String.IsNullOrEmpty(riderItem.UnitName))
                                {
                                    var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.UnitName));

                                    var newRow = dtRider.NewRow();
                                    newRow["RiderName"] = riderItem.Unit + " X " + riderType.TypeName + " (TT" + (i + 1) + ")";
                                    newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                                    newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                                    newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                                    dtRider.Rows.Add(newRow);
                                }
                            }
                            else if (riderItem.Rider.Category.Equals("Choice"))
                            {
                                if (!String.IsNullOrEmpty(riderItem.Choice))
                                {
                                    var riderType = rider.RiderTypes.FirstOrDefault(x => x.RiderTypeId == Convert.ToInt32(riderItem.Choice));

                                    var newRow = dtRider.NewRow();
                                    newRow["RiderName"] = riderItem.Rider.RiderName.Replace("Hanwha ","").Trim() + " - " + riderType.TypeName + " (TT" + (i + 1) + ")";
                                    newRow["MasaPertanggungan"] = riderType.CoverTerm / 12;
                                    newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                                    newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                                    dtRider.Rows.Add(newRow);
                                }
                            }
                            else
                            {
                                var newRow = dtRider.NewRow();
                                newRow["RiderName"] = riderItem.Rider.RiderName + " (TT" + (i + 1) + ")";
                                newRow["MasaPertanggungan"] = rider.CoverTerm / 12;
                                newRow["UangPertanggungan"] = riderItem.UangPertanggungan;
                                newRow["BiayaAsuransi"] = riderItem.BiayaAsuransi;
                                dtRider.Rows.Add(newRow);
                            }
                        }

                        if (dataInput.Additional.TertanggungTambahan.Count > 1)
                            y = y + dataInput.Additional.TertanggungTambahan.Count;
                        else
                            y++;
                    }
                }
            }

            dtRider.DefaultView.Sort = "RiderName";

            return dtRider;
        }

        public static DataTable FundInvestment(SummaryViewModel dataInput, List<Fund> fundList)
        {
            var dtFund = new DataTable { TableName = "FundInvestment" };

            dtFund.Columns.Add("FundName");
            dtFund.Columns.Add("Komposisi");
            dtFund.Columns.Add("Rendah");
            dtFund.Columns.Add("Sedang");
            dtFund.Columns.Add("Tinggi");

            foreach (var fundItem in dataInput.Premi.Investments.Where(x => x.Percentage != 0))
            {
                var fund = fundList.FirstOrDefault(x => x.FundCode == fundItem.InvestmentCode);

                var newRow = dtFund.NewRow();
                newRow["FundName"] = fund.FundName;
                newRow["Komposisi"] = fundItem.Percentage + "%";
                newRow["Rendah"] = fund.LowRate + "%";
                newRow["Sedang"] = fund.MediumRate + "%";
                newRow["Tinggi"] = fund.HighRate + "%";
                dtFund.Rows.Add(newRow);
            }

            return dtFund;
        }

        public static DataTable AllocationRate(List<ProductAllocationRate> productAllocationRates, Product product)
        {
            var dtAllocation = new DataTable { TableName = "AllocationRate" };

            dtAllocation.Columns.Add("Tahun");
            dtAllocation.Columns.Add("PremiInvestasi");
            dtAllocation.Columns.Add("PremiAkuisisi");
            dtAllocation.Columns.Add("TopUpInvestasi");
            dtAllocation.Columns.Add("TopUpAkuisisi");
            dtAllocation.Columns.Add("TopUpTunggalInvestasi");

            foreach (var rate in productAllocationRates)
            {
                var newRow = dtAllocation.NewRow();

                if (rate.Year == 6)
                    newRow["Tahun"] = rate.Year + " dst";
                else
                    newRow["Tahun"] = rate.Year;

                newRow["PremiInvestasi"] = rate.RatePremi + "%";
                newRow["PremiAkuisisi"] = (100 - rate.RatePremi) + "%";
                newRow["TopUpInvestasi"] = product.RegularTopUpRate + "%";
                newRow["TopUpAkuisisi"] = (100 - product.RegularTopUpRate) + "%";
                newRow["TopUpTunggalInvestasi"] = rate.RateSingleTopUp + "%";

                dtAllocation.Rows.Add(newRow);
            }

            return dtAllocation;
        }

        public static DataTable AllocationRateSingle(List<ProductAllocationRate> productAllocationRates, Product product)
        {
            var dtAllocation = new DataTable { TableName = "AllocationRateSingle" };

            dtAllocation.Columns.Add("Premi");
            dtAllocation.Columns.Add("Investasi");
            dtAllocation.Columns.Add("Akuisisi");

            var rate = productAllocationRates.FirstOrDefault();

            var newRow = dtAllocation.NewRow();
            newRow["Premi"] = App_Data.Text.PremiTunggal;
            newRow["Investasi"] = rate.RatePremi + "%";
            newRow["Akuisisi"] = (100 - rate.RatePremi) + "%";
            dtAllocation.Rows.Add(newRow);

            newRow = dtAllocation.NewRow();
            newRow["Premi"] = App_Data.Text.PremiTopUpTunggal;
            newRow["Investasi"] = rate.RateSingleTopUp + "%";
            newRow["Akuisisi"] = (100 - rate.RateSingleTopUp) + "%";
            dtAllocation.Rows.Add(newRow);

            return dtAllocation;
        }

        public static DataTable DataInput1(SummaryViewModel dataInput)
        {
            var dtData = new DataTable { TableName = "DataInput1" };
            dtData.Columns.Add("DataType");
            dtData.Columns.Add("Data");

            string insName;
            string age;
            string gender;
            string relation = "";

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
            {
                insName = dataInput.Nasabah.NamaPemegangPolis;
                age = dataInput.Nasabah.UmurPemegangPolis.ToString();
                gender = dataInput.Nasabah.JenisKelaminPemegangPolis;
            }
            else
            {
                insName = dataInput.Nasabah.NamaTertanggungUtama;
                age = dataInput.Nasabah.UmurTertanggungUtama.ToString();
                gender = dataInput.Nasabah.JenisKelaminTertanggungUtama;
                relation = dataInput.Nasabah.Relation;
            }

            var newRow = dtData.NewRow();
            newRow["DataType"] = App_Data.Text.NamaTertanggung;
            newRow["Data"] = insName;
            dtData.Rows.Add(newRow);

            newRow = dtData.NewRow();
            newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
            newRow["Data"] = age + App_Data.Text.TahunSpasi + " / " + (gender == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
            dtData.Rows.Add(newRow);

            newRow = dtData.NewRow();
            newRow["DataType"] = App_Data.Text.Hubungan;
            newRow["Data"] = !String.IsNullOrEmpty(relation) ? relation : App_Data.Text.DiriSendiri;
            dtData.Rows.Add(newRow);

            if (dataInput.Nasabah.NamaProduk != "HLSIN" && dataInput.Nasabah.NamaProduk != "HLEDU")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                newRow["Data"] = dataInput.Premi.RencanaMasaPembayaran + App_Data.Text.TahunSpasi;
                dtData.Rows.Add(newRow);
            }

            if (dataInput.Nasabah.NamaProduk == "HLSIN" || dataInput.Nasabah.NamaProduk == "HLEDU")
            {
                newRow = dtData.NewRow();
                newRow["DataType"] = App_Data.Text.MasaPembayaranPremi;
                if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
                {
                    newRow["Data"] = App_Data.Text.Sekaligus;
                }
                else
                {
                    var payTerm = dataInput.Premi.PilihanMasaPembayaran == "18" ? (Convert.ToInt32(dataInput.Premi.PilihanMasaPembayaran) - dataInput.Nasabah.UmurAnak).ToString() : dataInput.Premi.PilihanMasaPembayaran;
                    newRow["Data"] = payTerm + App_Data.Text.TahunSpasi;
                }
                dtData.Rows.Add(newRow);
            }

            newRow = dtData.NewRow();
            newRow["DataType"] = App_Data.Text.NamaAnak;
            newRow["Data"] = dataInput.Nasabah.NamaAnak;
            dtData.Rows.Add(newRow);

            newRow = dtData.NewRow();
            newRow["DataType"] = App_Data.Text.Usia + " / " + App_Data.Text.JenisKelamin;
            newRow["Data"] = dataInput.Nasabah.UmurAnak + App_Data.Text.TahunSpasi + " / " + (dataInput.Nasabah.JenisKelaminAnak == "Pria" ? App_Data.Text.Pria : App_Data.Text.Wanita);
            dtData.Rows.Add(newRow);

            return dtData;
        }

        public static DataTable DataInput2(SummaryViewModel dataInput)
        {
            var dtData = new DataTable { TableName = "DataInput2" };
            dtData.Columns.Add("Caption");
            dtData.Columns.Add("Value");

            var newRow = dtData.NewRow();
            newRow["Caption"] = App_Data.Text.MataUang;
            newRow["Value"] = dataInput.Premi.MataUang == "IDR" ? App_Data.Text.Rupiah : App_Data.Text.Rupiah;
            dtData.Rows.Add(newRow);

            newRow = dtData.NewRow();
            newRow["Caption"] = App_Data.Text.UangPertanggungan;
            newRow["Value"] = dataInput.StrCurrency + dataInput.Premi.UangPertanggungan;
            dtData.Rows.Add(newRow);

            var total = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);
            string caraBayar, berkala;

            if (dataInput.Premi.CaraBayar == "1")
            {
                caraBayar = App_Data.Text.Tahunan;
                berkala = App_Data.Text.TahunSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "2")
            {
                caraBayar = App_Data.Text.Semesteran;
                berkala = App_Data.Text.SemeterSpasi;
            }
            else if (dataInput.Premi.CaraBayar == "4")
            {
                caraBayar = App_Data.Text.Kuartalan;
                berkala = App_Data.Text.KuartalSpasi;
            }
            else
            {
                caraBayar = App_Data.Text.Bulanan;
                berkala = App_Data.Text.BulanSpasi;
            }

            newRow = dtData.NewRow();
            newRow["Caption"] = dataInput.Nasabah.NamaProduk == "HLEDU" ? App_Data.Text.PremiAsuransiDasar : App_Data.Text.PremiDasar;

            if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
            {
                newRow["Value"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala ;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = App_Data.Text.PremiAsuransiTambahan;
                newRow["Value"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan ;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = App_Data.Text.TotalPremi;
                newRow["Value"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total) ;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = dataInput.Nasabah.NamaProduk == "HLEDU" ? App_Data.Text.CaraPembayaranPremi : App_Data.Text.CaraPembayaran;
                newRow["Value"] = App_Data.Text.Sekaligus;
                dtData.Rows.Add(newRow);
            }
            else
            {
                newRow["Value"] = dataInput.StrCurrency + dataInput.Premi.PremiBerkala + " /" + berkala;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = App_Data.Text.PremiAsuransiTambahan;
                newRow["Value"] = dataInput.StrCurrency + dataInput.Rider.BiayaAsuransiTambahan + " /" + berkala;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = App_Data.Text.TotalPremi;
                newRow["Value"] = dataInput.StrCurrency + CalculatorServices.DecimalToCurrency(total) + " /" + berkala;
                dtData.Rows.Add(newRow);

                newRow = dtData.NewRow();
                newRow["Caption"] = dataInput.Nasabah.NamaProduk == "HLEDU" ? App_Data.Text.CaraPembayaranPremi : App_Data.Text.CaraPembayaran;
                newRow["Value"] = caraBayar;
                dtData.Rows.Add(newRow);
            }

            return dtData;
        }

        public static DataTable DanaTahapan(List<ProductTahapanRate> tahapanRates)
        {
            var dtTahapan = new DataTable { TableName = "DanaTahapan" };
            dtTahapan.Columns.AddRange(new[] { new DataColumn("UsiaAnak"), new DataColumn("Percentage") });

            foreach (var t in tahapanRates)
            {
                var fund = dtTahapan.NewRow();
                fund["UsiaAnak"] = t.Year;
                fund["Percentage"] = t.RateTahapan + "%";
                dtTahapan.Rows.Add(fund);
            }

            return dtTahapan;
        }

        public static DataTable CreateRingkasanRegular(DataTable dtFund, SummaryViewModel dataInput)
        {
            var dtRegular = new DataTable { TableName = "InvestBenefit" };

            dtRegular.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("Usia"),

                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopUp"),
                new DataColumn("Withdrawal"),

                new DataColumn("Rendah"),
                new DataColumn("Sedang"),
                new DataColumn("Tinggi"),

                new DataColumn("PremiAllTime"),
                new DataColumn("RendahAllTime"),
                new DataColumn("SedangAllTime"),
                new DataColumn("TinggiAllTime"),

                new DataColumn("MatiRendah"),
                new DataColumn("MatiSedang"),
                new DataColumn("MatiTinggi"),

                new DataColumn("MatiRendahAllTime"),
                new DataColumn("MatiSedangAllTime"),
                new DataColumn("MatiTinggiAllTime")
            });

            if (dtFund == null) return dtRegular;
            var investments = dataInput.Premi.Investments;

            for (int i = 0; i < dtFund.Rows.Count; i++)
            {
                var tahunKe = Convert.ToInt32(dtFund.Rows[i]["Tahun"]);
                var usiaKe = Convert.ToInt32(dtFund.Rows[i]["Usia"]);

                if (tahunKe == 26)
                {
                    var drSkip = dtRegular.NewRow();

                    drSkip["Tahun"] = ":";
                    drSkip["Usia"] = ":";
                    drSkip["UangPertanggungan"] = ":";
                    drSkip["PremiPerTahun"] = ":";
                    drSkip["TopUp"] = ":";
                    drSkip["Withdrawal"] = ":";
                    drSkip["PremiAllTime"] = ":";
                    drSkip["Rendah"] = ":";
                    drSkip["Sedang"] = ":";
                    drSkip["Tinggi"] = ":";
                    drSkip["RendahAllTime"] = ":";
                    drSkip["SedangAllTime"] = ":";
                    drSkip["TinggiAllTime"] = ":";
                    drSkip["MatiRendah"] = ":";
                    drSkip["MatiSedang"] = ":";
                    drSkip["MatiTinggi"] = ":";
                    drSkip["MatiRendahAllTime"] = ":";
                    drSkip["MatiSedangAllTime"] = ":";
                    drSkip["MatiTinggiAllTime"] = ":";

                    dtRegular.Rows.Add(drSkip);
                }

                if (dtRegular.Rows.Count >= 26)
                {
                    if (usiaKe < 55) continue;
                    if (usiaKe >= 56 && usiaKe < 60) continue;
                    if (usiaKe >= 61 && usiaKe < 65) continue;
                    if (usiaKe >= 66 && usiaKe < 70) continue;
                    if (usiaKe >= 71 && usiaKe < 75) continue;
                    if (usiaKe >= 76 && usiaKe < 80) continue;
                    if (usiaKe >= 81 && usiaKe < 85) continue;
                    if (usiaKe >= 86 && usiaKe < 90) continue;
                    if (usiaKe >= 91 && usiaKe < 95) continue;
                    if (usiaKe >= 96 && usiaKe < 99) continue;
                }

                var dr = dtRegular.NewRow();
                dr["Tahun"] = tahunKe;
                dr["Usia"] = usiaKe;

                dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"])) / 1000) : "";
                dr["PremiPerTahun"] = dtFund.Rows[i]["PremiPerTahun"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiPerTahun"])) / 1000) : "";
                
                dr["TopUp"] = dtFund.Rows[i]["TopupPremi"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal((string)dtFund.Rows[i]["TopupPremi"])) / 1000) : "";
                
                dr["Withdrawal"] = dtFund.Rows[i]["Withdrawal"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(CalculatorServices.CurrencyToDecimal((string)dtFund.Rows[i]["Withdrawal"])) / 1000) : "";

                if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    dr["PremiAllTime"] = dtFund.Rows[i]["PremiPerTahunSeumurHidup"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiPerTahunSeumurHidup"])) / 1000) : "";

                var invRendah = 0m;
                var invSedang = 0m;
                var invTinggi = 0m;

                var invAllTimeRendah = 0m;
                var invAllTimeSedang = 0m;
                var invAllTimeTinggi = 0m;

                foreach (var fundType in investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
                {
                    invRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_low"]));
                    invSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_moderate"]));
                    invTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_fund_high"]));

                    if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    {
                        invAllTimeRendah += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_low"]));
                        invAllTimeSedang += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_moderate"]));
                        invAllTimeTinggi += Math.Round(Convert.ToDecimal(dtFund.Rows[i][fundType + "_alltime_fund_high"]));
                    }
                }

                dr["Rendah"] = invRendah >= 0 ? CalculatorServices.DecimalToCurrency(invRendah / 1000) : "*****";
                dr["Sedang"] = invSedang >= 0 ? CalculatorServices.DecimalToCurrency(invSedang / 1000) : "*****";
                dr["Tinggi"] = invTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invTinggi / 1000) : "*****";

                if (dataInput.Nasabah.NamaProduk != "HLSIN")
                {
                    dr["RendahAllTime"] = invAllTimeRendah >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeRendah / 1000) : "*****";
                    dr["SedangAllTime"] = invAllTimeSedang >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeSedang / 1000) : "*****";
                    dr["TinggiAllTime"] = invAllTimeTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeTinggi / 1000) : "*****";
                }

                if (dtFund.Rows[i]["UangPertanggungan"] != null)
                {
                    var up = Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"]);

                    var invRendahDeath = invRendah + up;
                    var invSedangDeath = invSedang + up;
                    var invTinggiDeath = invTinggi + up;

                    dr["MatiRendah"] = invRendah >= 0 ? CalculatorServices.DecimalToCurrency(invRendahDeath / 1000) : "*****";
                    dr["MatiSedang"] = invSedang >= 0 ? CalculatorServices.DecimalToCurrency(invSedangDeath / 1000) : "*****";
                    dr["MatiTinggi"] = invTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invTinggiDeath / 1000) : "*****";

                    if (dataInput.Nasabah.NamaProduk != "HLSIN")
                    {
                        var invAllTimeRendahDeath = invAllTimeRendah + up;
                        var invAllTimeSedangDeath = invAllTimeSedang + up;
                        var invAllTimeTinggiDeath = invAllTimeTinggi + up;

                        dr["MatiRendahAllTime"] = invAllTimeRendah >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeRendahDeath / 1000) : "*****";
                        dr["MatiSedangAllTime"] = invAllTimeSedang >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeSedangDeath / 1000) : "*****";
                        dr["MatiTinggiAllTime"] = invAllTimeTinggi >= 0 ? CalculatorServices.DecimalToCurrency(invAllTimeTinggiDeath / 1000) : "*****";
                    }
                }

                dtRegular.Rows.Add(dr);
            }

            return dtRegular;
        }

        public static DataTable CreateTraditionalHeader(SummaryViewModel dataInput)
        {
            var dt = new DataTable { TableName = "InvestBenefitTraditionalHeader" };

            dt.Columns.Add(new DataColumn("Data"));
            var dtRow = dt.NewRow();
            dtRow["Data"] = dataInput.Premi.ModeBayarPremi.Equals("sekaligus") ? App_Data.Text.PremiDasar : App_Data.Text.PremiTahunan;
            dt.Rows.Add(dtRow);
            return dt;
        }

        public static DataTable CreateRingkasanTraditional(DataTable dtFund)
        {
            var dtTraditional = new DataTable { TableName = "InvestBenefitTraditional" };

            dtTraditional.Columns.AddRange(new[]{
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            if (dtFund == null) return dtTraditional;

            for (int i = 0; i < dtFund.Rows.Count; i++)
            {
                var tahunKe = Convert.ToInt32(dtFund.Rows[i]["Tahun"]);
                var usiaKe = Convert.ToInt32(dtFund.Rows[i]["UsiaTertanggung"]);
                var usiaAnakKe = Convert.ToInt32(dtFund.Rows[i]["UsiaAnak"]);

                if (tahunKe == 23)
                {
                    var drSkip = dtTraditional.NewRow();

                    drSkip["Tahun"] = ":";
                    drSkip["UsiaTertanggung"] = ":";
                    drSkip["UsiaAnak"] = ":";
                    drSkip["UsiaTertanggung"] = ":";
                    drSkip["PremiTahunan"] = ":";
                    drSkip["DanaTahapan"] = ":";
                    drSkip["AkumulasiDanaTahapan"] = ":";
                    drSkip["Reserve"] = ":";
                    drSkip["CashValue"] = ":";
                    drSkip["DeathBenefit"] = ":";
                    drSkip["Total"] = ":";

                    dtTraditional.Rows.Add(drSkip);
                }

                if (dtTraditional.Rows.Count >= 23)
                {
                    if (usiaAnakKe < 25) continue;
                    if (usiaAnakKe >= 26 && usiaAnakKe < 30) continue;
                    if (usiaAnakKe >= 31 && usiaAnakKe < 35) continue;
                    if (usiaAnakKe >= 36 && usiaAnakKe < 40) continue;
                }

                var dr = dtTraditional.NewRow();
                dr["Tahun"] = tahunKe;
                dr["UsiaTertanggung"] = usiaKe;
                dr["UsiaAnak"] = usiaAnakKe;

                dr["UangPertanggungan"] = dtFund.Rows[i]["UangPertanggungan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["UangPertanggungan"])) / 1000) : "";
                dr["PremiTahunan"] = dtFund.Rows[i]["PremiTahunan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["PremiTahunan"])) / 1000) : "";
                dr["DanaTahapan"] = dtFund.Rows[i]["DanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DanaTahapan"])) / 1000) : "";
                dr["AkumulasiDanaTahapan"] = dtFund.Rows[i]["AkumulasiDanaTahapan"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["AkumulasiDanaTahapan"])) / 1000) : "";
                dr["Reserve"] = dtFund.Rows[i]["Reserve"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Reserve"])) / 1000) : "";
                dr["CashValue"] = dtFund.Rows[i]["CashValue"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["CashValue"])) / 1000) : "";
                dr["DeathBenefit"] = dtFund.Rows[i]["DeathBenefit"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["DeathBenefit"])) / 1000) : "";
                dr["Total"] = dtFund.Rows[i]["Total"] != DBNull.Value ? CalculatorServices.DecimalToCurrency(Math.Round(Convert.ToDecimal(dtFund.Rows[i]["Total"])) / 1000) : "";

                dtTraditional.Rows.Add(dr);
            }

            return dtTraditional;
        }

        public static DataTable CalculateFundBenefitRegular(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var totalPremiTambahanTahunIni = 0;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe == 1)
                    {
                        biayaAdminTahunIni = 0;
                        biayaAsuransiTahunPertama = biayaAsuransiTahunIni;
                        biayaAsuransiTahunIni = 0;
                    }

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        if (tahunKe == 2 || tahunKe == 3)
                        {
                            biayaAdminTahunIni *= 1.5m;
                            biayaAsuransiTahunIni += (decimal)(biayaAsuransiTahunPertama / 2);
                            lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                        }
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                        monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }



            if ((dataInput.Nasabah.TertanggungUtama == "Ya" && dataInput.Nasabah.UmurPemegangPolis <= 17) ||
                (dataInput.Nasabah.TertanggungUtama != "Ya" && dataInput.Nasabah.UmurTertanggungUtama <= 17))
            {
                if (CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan) > 1500000000)
                {
                    errorMsgList.Add("Maksimum Uang Pertanggungan untuk Tertanggung Utama Anak (Juvenile) adalah Rp 1.500.000.000");
                }
            }

            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}


            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderRegular(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;
                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                decimal biayaAsuransiTambahan = 0;
                decimal biayaAsuransiTambahanNotDeferred = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;

                    // Hanwha Wizer cov term 70 year
                    if (product.ProductCode.Equals("HLWIZ"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;

                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);

                            if (riderItem.Rider.Category.Equals("Choice"))
                                biayaAsuransiTambahanNotDeferred += cor;
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;
                decimal biayaAsuransiTertanggungTambahanNotDeferred = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;

                                // Hanwha Wizer cov term 70 year
                                if (product.ProductCode.Equals("HLWIZ"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;

                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);

                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            biayaAsuransiTertanggungTambahanNotDeferred += cor;
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                decimal? totalPremiTambahanTahunIni = biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe == 1)
                    {
                        biayaAdminTahunIni = 0;
                        biayaAsuransiTahunPertama = biayaAsuransiTahunIni - (biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred);
                        biayaAsuransiTahunIni = 0 + biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred;
                    }

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        if (tahunKe == 2 || tahunKe == 3)
                        {
                            biayaAdminTahunIni *= 1.5m;
                            biayaAsuransiTahunIni += (biayaAsuransiTahunPertama / 2);
                            lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                        }
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                        monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }
            }


            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderAndTopupWithdrawalRegular(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high"),

                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high"),

                    new DataColumn(fundType + "_alltime_death_low"),
                    new DataColumn(fundType + "_alltime_death_moderate"),
                    new DataColumn(fundType + "_alltime_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            decimal? biayaAsuransiTahunPertama = 0;

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                TopUpWithdrawal topupTahunIni = null;
                TopUpWithdrawal withdrawalTahunIni = null;
                decimal biayaAsuransiTambahan = 0;
                decimal biayaAsuransiTambahanNotDeferred = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;

                    // Hanwha Wizer cov term 70 year
                    if (product.ProductCode.Equals("HLWIZ"))
                        riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;

                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);

                            if (riderItem.Rider.Category.Equals("Choice"))
                                biayaAsuransiTambahanNotDeferred += cor;
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;
                decimal biayaAsuransiTertanggungTambahanNotDeferred = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;

                                // Hanwha Wizer cov term 70 year
                                if (product.ProductCode.Equals("HLWIZ"))
                                    riderItem.Rider.CoverTerm = riderItem.Rider.CoverTermWizer;

                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);

                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            biayaAsuransiTertanggungTambahanNotDeferred += cor;
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    topupTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => x.IsTopUp && x.Year == tahunKe);
                    withdrawalTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => !x.IsTopUp && x.Year == tahunKe);
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalTopUpTahunIni = topupTahunIni != null ? CalculatorServices.CurrencyToDecimal(topupTahunIni.Amount) * (decimal)(allocationRate.RateSingleTopUp / 100) : 0;
                var totalWithdrawalTahunIni = withdrawalTahunIni != null ? CalculatorServices.CurrencyToDecimal(withdrawalTahunIni.Amount) : 0;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);

                decimal? totalPremiTambahanTahunIni = biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;

                if (topupTahunIni != null) drFund["TopupPremi"] = topupTahunIni.Amount;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;
                if (withdrawalTahunIni != null) drFund["Withdrawal"] = withdrawalTahunIni.Amount;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentBiayaAsuransi = 0;
                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe == 1)
                    {
                        if (product.ProductCode.Equals("HLWIZ"))
                        {
                            biayaAsuransiTahunPertama = biayaAsuransiTahunIni - (biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred);
                        }
                        else
                        {
                            biayaAdminTahunIni = 0;
                            biayaAsuransiTahunPertama = biayaAsuransiTahunIni - (biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred);
                            biayaAsuransiTahunIni = 0 + biayaAsuransiTambahanNotDeferred + biayaAsuransiTertanggungTambahanNotDeferred;
                        }

                    }

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);

                        if (tahunKe == 2 || tahunKe == 3)
                        {
                            if (product.ProductCode.Equals("HLWIZ"))
                            {
                                lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                            }
                            else
                            {
                                biayaAdminTahunIni *= 1.5m;
                                biayaAsuransiTahunIni += (biayaAsuransiTahunPertama / 2);
                                lastYearInvestmentBiayaAsuransi = Convert.ToDecimal(dtFundBenefit.Rows[0][fundType + "_fund_as"]) / 2;
                            }

                        }
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }                        

                        if (iMonth == 1)
                        {
                            monthFundLow = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMed = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHigh = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                            monthFundLowAllTime = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMedAllTime = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHighAllTime = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                        }
                        else if (iMonth == 12)
                        {
                            monthFundLow = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal);

                            monthFundMed = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal);

                            monthFundHigh = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal);

                            monthFundLowAllTime = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal);

                            monthFundMedAllTime = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal);

                            monthFundHighAllTime = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal);
                        }
                        else
                        {
                            monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                            monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                        }
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            if (product.MinRemainingBalance.HasValue)
            {
                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    foreach (var withdrawal in dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false))
                    {
                        //validasi Wizer
                        //if(product.MinWithdrawalBalance.HasValue && product.ProductCode == "HLWIZ" && display)
                        //{
                        //    var totalPremi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiAngsuran) * withdrawal.Year;
                        //    if (withdrawal.Year == 2)
                        //        totalPremi = 90 * totalPremi / 100;

                        //    var cekEstimasiInvestasi = Math.Min(totalPremi, product.MinWithdrawalBalance.Value);

                        //    var tahunAllCekWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year);
                        //    if (withdrawal.Year <= 2 && tahunAllCekWithdrawal != null && tahunAllCekWithdrawal.Low < cekEstimasiInvestasi)
                        //    {
                        //        display = false;
                        //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)cekEstimasiInvestasi) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        //    }

                        //    tahunAllCekWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year && x.Low < product.MinWithdrawalBalance);
                        //    if(withdrawal.Year > 2 && tahunAllCekWithdrawal != null && display)
                        //    {
                        //        display = false;
                        //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)product.MinWithdrawalBalance) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        //    }
                        //}
                        // end

                        var remainingBalanceAfterWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year && x.Med < product.MinRemainingBalance);

                        if (remainingBalanceAfterWithdrawal != null && display)
                        {
                            display = false;
                            errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)product.MinRemainingBalance) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        }
                    }
                }
            }

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitSingle(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("Premi"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var totalAlokasiPremi = premiSekaligus * (decimal)(allocationRate.RatePremi / 100) + topupSekaligus * (decimal)(product.RegularTopUpRate / 100);
                var thisYearPremi = tahunKe == 1 ? totalAlokasiPremi : 0m;

                var coi = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = up;
                if (thisYearPremi > 0) drFund["Premi"] = premiSekaligus;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundHigh = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = coi;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? totalAlokasiPremiBulanan = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            totalAlokasiPremiBulanan = thisYearPremi * fundAllocationInPoint;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    drFund[fundType + "_fund_as"] = biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }


            if ((dataInput.Nasabah.TertanggungUtama == "Ya" && dataInput.Nasabah.UmurPemegangPolis <= 17) ||
                (dataInput.Nasabah.TertanggungUtama != "Ya" && dataInput.Nasabah.UmurTertanggungUtama <= 17))
            {
                if (CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan) > 1500000000)
                {
                    errorMsgList.Add("Maksimum Uang Pertanggungan untuk Tertanggung Utama Anak (Juvenile) adalah Rp 1.500.000.000");
                }
            }


            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderSingle(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("Premi"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;
                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                decimal biayaAsuransiTambahan = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;
                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;
                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var totalAlokasiPremi = premiSekaligus * (decimal)(allocationRate.RatePremi / 100) + topupSekaligus * (decimal)(product.RegularTopUpRate / 100);
                var thisYearPremi = tahunKe == 1 ? totalAlokasiPremi : 0m;

                var coi = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = up;
                if (thisYearPremi > 0) drFund["Premi"] = premiSekaligus;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundHigh = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = coi + biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? totalAlokasiPremiBulanan = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            totalAlokasiPremiBulanan = thisYearPremi * fundAllocationInPoint;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    drFund[fundType + "_fund_as"] = biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }

            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWithRiderAndTopupWithdrawalSingle(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, List<RiderRate> riderRateList, List<RiderRate> riderRateAddList, List<AnnuityFactor> annuityFactorList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupSekaligus = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;
                var phAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
                phAge += tahunKe;
                ProductAllocationRate allocationRate;
                TopUpWithdrawal topupTahunIni = null;
                TopUpWithdrawal withdrawalTahunIni = null;
                decimal biayaAsuransiTambahan = 0;

                #region Rider
                foreach (var riderItem in dataInput.Rider.Riders.Where(x => x.Checked && x.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(x.BiayaAsuransi) > 0))
                {
                    var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                    var riderCode = riderItem.Rider.RiderCode;
                    int? riderType = null;
                    var riderCategory = riderItem.Rider.Category;
                    var riskClass = 0;
                    decimal cor = 0;
                    var covTerm = riderItem.Rider.CoverTerm / 12;

                    if ((i - 1) < covTerm && riderCode != "POP")
                    {
                        if (dataInput.Nasabah.TertanggungUtama == "Ya")
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanPemegangPolis);
                        else
                            riskClass = Convert.ToInt32(dataInput.Nasabah.KelasPekerjaanTertanggungUtama);

                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }
                        else
                        {
                            if (riderItem.Rider.Category.Equals("Choice"))
                                riderType = Convert.ToInt32(riderItem.Choice);

                            if (riderItem.Rider.Category.Equals("Unit"))
                            {
                                riderType = Convert.ToInt32(riderItem.UnitName);
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                            }
                            else
                                cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (i - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                        }

                        biayaAsuransiTambahan += cor;
                    }
                    else if (riderCode == "POP" && (phAge - 1) < covTerm)
                    {
                        if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                        {
                            foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                            {
                                riderType = choice.RiderType.RiderTypeId;
                                cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, (phAge - 1), riskClass, riderCategory, product, riderRateList, annuityFactorList, null);
                            }
                        }

                        biayaAsuransiTambahan += cor;
                    }
                }
                #endregion

                decimal biayaAsuransiTertanggungTambahan = 0;

                #region Additional Insured
                if (dataInput.Additional.TertanggungTambahan != null)
                {
                    for (var iAddIns = 0; iAddIns < dataInput.Additional.TertanggungTambahan.Count; iAddIns++)
                    {
                        for (var iRiderAddIns = iAddIns; iRiderAddIns < dataInput.Additional.Riders.Count;)
                        {
                            var riderItem = dataInput.Additional.Riders[iRiderAddIns];

                            if (riderItem.Checked || (riderItem.BiayaAsuransi != null && CalculatorServices.CurrencyToDecimal(riderItem.BiayaAsuransi) > 0) || riderItem.Rider.Category.Equals("Choices"))
                            {
                                var upRider = CalculatorServices.CurrencyToDecimal(riderItem.UangPertanggungan);
                                var riderCode = riderItem.Rider.RiderCode;
                                int? riderType = null;
                                var riderCategory = riderItem.Rider.Category;
                                var riskClass = 0;
                                decimal cor = 0;
                                var covTerm = riderItem.Rider.CoverTerm / 12;
                                int ageAddIns = (int)dataInput.Additional.TertanggungTambahan[iAddIns].Age + (tahunKe - 1);

                                if (ageAddIns < covTerm)
                                {
                                    riskClass = Convert.ToInt32(dataInput.Additional.TertanggungTambahan[iAddIns].RiskClass);

                                    if (riderItem.Choices != null && riderItem.Choices.Count > 0)
                                    {
                                        foreach (var choice in riderItem.Choices.Where(x => x.Checked))
                                        {
                                            riderType = choice.RiderType.RiderTypeId;
                                            cor += CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                        }
                                    }
                                    else
                                    {
                                        if (riderItem.Rider.Category.Equals("Choice"))
                                            riderType = Convert.ToInt32(riderItem.Choice);

                                        if (riderItem.Rider.Category.Equals("Unit"))
                                        {
                                            riderType = Convert.ToInt32(riderItem.UnitName);
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, Convert.ToInt32(riderItem.Unit));
                                        }
                                        else
                                            cor = CalculatorServices.CalculateCostOfRider(dataInput, upRider, riderCode, riderType, ageAddIns, riskClass, riderCategory, product, riderRateAddList, annuityFactorList, null);
                                    }

                                    biayaAsuransiTertanggungTambahan += cor;
                                }
                            }

                            if (dataInput.Additional.TertanggungTambahan.Count > 1)
                                iRiderAddIns += dataInput.Additional.TertanggungTambahan.Count;
                            else
                                iRiderAddIns++;
                        }
                    }
                }
                #endregion

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var totalAlokasiPremi = premiSekaligus * (decimal)(allocationRate.RatePremi / 100) + topupSekaligus * (decimal)(product.RegularTopUpRate / 100);
                var thisYearPremi = tahunKe == 1 ? totalAlokasiPremi : 0m;

                var coi = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    topupTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => x.IsTopUp && x.Year == tahunKe);
                    withdrawalTahunIni = dataInput.TopUp.TopupWithdrawals.FirstOrDefault(x => !x.IsTopUp && x.Year == tahunKe);
                }

                var totalTopUpTahunIni = topupTahunIni != null ? CalculatorServices.CurrencyToDecimal(topupTahunIni.Amount) * (decimal)(allocationRate.RateSingleTopUp / 100) : 0;
                var totalWithdrawalTahunIni = withdrawalTahunIni != null ? CalculatorServices.CurrencyToDecimal(withdrawalTahunIni.Amount)  : 0;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = up;
                if (thisYearPremi > 0) drFund["PremiPerTahun"] = premiSekaligus + (dataInput.Nasabah.NamaProduk == "HLSIN" ? topupSekaligus : 0);
                if (topupTahunIni != null) drFund["TopupPremi"] = topupTahunIni.Amount;
                if (withdrawalTahunIni != null) drFund["Withdrawal"] = withdrawalTahunIni.Amount;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundHigh = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = coi + biayaAsuransiTambahan + biayaAsuransiTertanggungTambahan;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? totalAlokasiPremiBulanan = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            totalAlokasiPremiBulanan = thisYearPremi * fundAllocationInPoint;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                        }

                        if (iMonth == 1)
                        {
                            monthFundLow = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMed = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHigh = CalculatorServices.CalculateFundWithTopup(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalTopUpTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                        }
                        else if (iMonth == 12)
                        {
                            monthFundLow = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)), allocationRate.RateWithdrawal);

                            monthFundMed = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)), allocationRate.RateWithdrawal);

                            monthFundHigh = CalculatorServices.CalculateFundWithdrawal(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, totalWithdrawalTahunIni, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)), allocationRate.RateWithdrawal);
                        }
                        else
                        {
                            monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                            monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                            monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                        }
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    drFund[fundType + "_fund_as"] = biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;

            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }

            }


            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            if (product.MinRemainingBalance.HasValue)
            {
                if (dataInput.TopUp.TopupWithdrawals != null)
                {
                    foreach (var withdrawal in dataInput.TopUp.TopupWithdrawals.Where(x => x.IsTopUp == false))
                    {
                        var remainingBalanceAfterWithdrawal = listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun == withdrawal.Year && x.Med < product.MinRemainingBalance);

                        if (remainingBalanceAfterWithdrawal != null && display)
                        {
                            display = false;
                            errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda kurang dari " + CalculatorServices.DecimalToCurrency((decimal)product.MinRemainingBalance) + " setelah withdrawal pada tahun ke " + withdrawal.Year);
                        }
                    }
                }
            }

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateFundBenefitWizer(SummaryViewModel dataInput, Product product, Dictionary<int?, double?> coiRates, List<ProductAllocationRate> productAllocationRates, List<Fund> investFunds, out string[] errorMsg)
        {
            errorMsg = null;

            var dtFundBenefit = new DataTable("FundBenefit");
            dtFundBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("Usia"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiPerTahun"),
                new DataColumn("TopupPremi"),
                new DataColumn("Withdrawal"),
                new DataColumn("PremiPerTahunSeumurHidup"),
                new DataColumn("TopupPremiSeumurHidup")
            });

            if (product == null) return dtFundBenefit;
            var covAge = product.CovAge / 12;

            foreach (var fundType in dataInput.Premi.Investments.Where(x => x.Percentage > 0).Select(x => x.InvestmentCode))
            {
                dtFundBenefit.Columns.AddRange(new[]
                {
                    new DataColumn(fundType + "_fund_as"),

                    new DataColumn(fundType + "_fund_low"),
                    new DataColumn(fundType + "_alltime_fund_low"),

                    new DataColumn(fundType + "_fund_moderate"),
                    new DataColumn(fundType + "_alltime_fund_moderate"),

                    new DataColumn(fundType + "_fund_high"),
                    new DataColumn(fundType + "_alltime_fund_high"),

                    new DataColumn(fundType + "_death_low"),
                    new DataColumn(fundType + "_death_moderate"),
                    new DataColumn(fundType + "_death_high"),

                    new DataColumn(fundType + "_alltime_death_low"),
                    new DataColumn(fundType + "_alltime_death_moderate"),
                    new DataColumn(fundType + "_alltime_death_high")
                });
            }

            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            var listFundTotalInvestmentPerYear = new List<InvestmentFundRow>();
            var listFundTotalInvestmentPerYearAllTime = new List<InvestmentFundRow>();
            var insAge = 0;
            decimal? premiBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala);
            decimal? topupBerkala = CalculatorServices.CurrencyToDecimal(dataInput.Premi.TopupBerkala);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= covAge; i++)
            {
                var tahunKe = i - insAge;

                ProductAllocationRate allocationRate;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == product.ProductCode);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var premiPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? (premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var topupBerkalaPerTahun = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? (topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar)) : 0;
                var premiPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premiBerkala : 0;
                var topupBerkalaPerTahunan = tahunKe <= dataInput.Premi.RencanaMasaPembayaran && topupBerkala.HasValue ? topupBerkala : 0;

                var premiBerkalaAllYear = premiBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var topupBerkalaAllYear = topupBerkala / Convert.ToInt32(dataInput.Premi.CaraBayar);
                var premiBerkalaAll = premiBerkala;
                var topupBerkalaAll = topupBerkala;

                var totalAlokasiPremiTahunIni = premiPerTahun * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaPerTahun * (decimal)(product.RegularTopUpRate / 100);
                var totalAlokasiPremiTahunIniAlwaysPremi = premiBerkalaAllYear * (decimal)(allocationRate.RatePremi / 100) + topupBerkalaAllYear * (decimal)(product.RegularTopUpRate / 100);
                var thisYearUangPertanggungan = up;

                var premiPerTahunInRow = premiPerTahunan + topupBerkalaPerTahunan;
                var premiPerTahunAlYearInRow = premiBerkalaAll + topupBerkalaAll;

                var premiAsuransiDasarTahunIni = CalculatorServices.CalculateCostOfInsurance(thisYearUangPertanggungan.ToString(), (double)coiRates[i - 1]);
                var premiAsuransiDasarTahunIniAllYear = CalculatorServices.CalculateCostOfInsurance(up.ToString(), (double)coiRates[i - 1]);

                var totalPremiTambahanTahunIni = 0;

                var drFund = dtFundBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["Usia"] = i;
                drFund["UangPertanggungan"] = thisYearUangPertanggungan;
                drFund["PremiPerTahunSeumurHidup"] = premiPerTahunAlYearInRow;
                if (premiPerTahun > 0) drFund["PremiPerTahun"] = premiPerTahunInRow;

                var investmentFundRow = new InvestmentFundRow { Tahun = tahunKe };
                var investmentFundRowAllTime = new InvestmentFundRow { Tahun = tahunKe };

                foreach (var invest in dataInput.Premi.Investments.Where(x => x.Percentage > 0))
                {
                    var fundType = invest.InvestmentCode.ToLower();
                    var invest1 = invest;
                    var fund = investFunds.FirstOrDefault(x => x.FundCode == invest1.InvestmentCode);

                    if (fund == null) continue;

                    decimal lastYearInvestmentFundLow = 0;
                    decimal lastYearInvestmentFundLowAlwaysPremi = 0;

                    decimal lastYearInvestmentFundMed = 0;
                    decimal lastYearInvestmentFundMedAlwaysPremi = 0;

                    decimal lastYearInvestmentFundHigh = 0;
                    decimal lastYearInvestmentFundHighAlwaysPremi = 0;

                    var fundAllocationInPoint = invest.Percentage / 100m;
                    var biayaAdminTahunIni = product.AdminFee * 12;
                    var biayaAsuransiTahunIni = totalPremiTambahanTahunIni + premiAsuransiDasarTahunIni;

                    if (tahunKe > 1)
                    {
                        lastYearInvestmentFundLow = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_low"]);
                        lastYearInvestmentFundMed = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_moderate"]);
                        lastYearInvestmentFundHigh = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_fund_high"]);

                        lastYearInvestmentFundLowAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_low"]);
                        lastYearInvestmentFundMedAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_moderate"]);
                        lastYearInvestmentFundHighAlwaysPremi = Convert.ToDecimal(dtFundBenefit.Rows[dtFundBenefit.Rows.Count - 1][fundType + "_alltime_fund_high"]);
                    }

                    decimal? fundLow = 0;
                    decimal? fundMed = 0;
                    decimal? fundHigh = 0;
                    decimal? fundLowAllTime = 0;
                    decimal? fundMedAllTime = 0;
                    decimal? fundHighAllTime = 0;
                    decimal? monthFundLow = 0;
                    decimal? monthFundMed = 0;
                    decimal? monthFundHigh = 0;
                    decimal? monthFundLowAllTime = 0;
                    decimal? monthFundMedAllTime = 0;
                    decimal? monthFundHighAllTime = 0;

                    for (int iMonth = 1; iMonth <= 12; iMonth++)
                    {
                        decimal? lastMonthInvestmentFundLow = 0;
                        decimal? lastMonthInvestmentFundMed = 0;
                        decimal? lastMonthInvestmentFundHigh = 0;
                        decimal? lastMonthInvestmentFundLowAllTime = 0;
                        decimal? lastMonthInvestmentFundMedAllTime = 0;
                        decimal? lastMonthInvestmentFundHighAllTime = 0;
                        decimal? totalAlokasiPremiBulanan = 0;
                        decimal? totalAlokasiPremiBulananAlwaysPremi = 0;

                        if (iMonth == 1)
                        {
                            lastMonthInvestmentFundLow = lastYearInvestmentFundLow;
                            lastMonthInvestmentFundMed = lastYearInvestmentFundMed;
                            lastMonthInvestmentFundHigh = lastYearInvestmentFundHigh;
                            lastMonthInvestmentFundLowAllTime = lastYearInvestmentFundLowAlwaysPremi;
                            lastMonthInvestmentFundMedAllTime = lastYearInvestmentFundMedAlwaysPremi;
                            lastMonthInvestmentFundHighAllTime = lastYearInvestmentFundHighAlwaysPremi;
                        }
                        else
                        {
                            lastMonthInvestmentFundLow = monthFundLow;
                            lastMonthInvestmentFundMed = monthFundMed;
                            lastMonthInvestmentFundHigh = monthFundHigh;
                            lastMonthInvestmentFundLowAllTime = monthFundLowAllTime;
                            lastMonthInvestmentFundMedAllTime = monthFundMedAllTime;
                            lastMonthInvestmentFundHighAllTime = monthFundHighAllTime;
                        }

                        var pm = dataInput.Premi.CaraBayar;
                        var pmMonth = pm + iMonth + "";
                        var pmMonthMap = CalculatorServices.PaymentMethodMapping();
                        bool result;
                        if (pmMonthMap.TryGetValue(pmMonth, out result))
                        {
                            if (pmMonthMap[pmMonth])
                            {
                                totalAlokasiPremiBulanan = totalAlokasiPremiTahunIni * fundAllocationInPoint;
                                totalAlokasiPremiBulananAlwaysPremi = totalAlokasiPremiTahunIniAlwaysPremi.Value * fundAllocationInPoint;
                            }
                        }

                        monthFundLow = CalculatorServices.CalculateFund(lastMonthInvestmentFundLow, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                                biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMed = CalculatorServices.CalculateFund(lastMonthInvestmentFundMed, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHigh = CalculatorServices.CalculateFund(lastMonthInvestmentFundHigh, totalAlokasiPremiBulanan, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));

                        monthFundLowAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundLowAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.LowRate)));

                        monthFundMedAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundMedAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.MediumRate)));

                        monthFundHighAllTime = CalculatorServices.CalculateFund(lastMonthInvestmentFundHighAllTime, totalAlokasiPremiBulananAlwaysPremi, biayaAdminTahunIni,
                            biayaAsuransiTahunIni, fundAllocationInPoint, (decimal)CalculatorServices.MonthlyFundRate(Convert.ToDouble(fund.HighRate)));
                    }

                    fundLow = monthFundLow;
                    fundMed = monthFundMed;
                    fundHigh = monthFundHigh;

                    fundLowAllTime = monthFundLowAllTime;
                    fundMedAllTime = monthFundMedAllTime;
                    fundHighAllTime = monthFundHighAllTime;

                    drFund[fundType + "_fund_as"] = tahunKe == 1 ? 0 : biayaAsuransiTahunIni;

                    drFund[fundType + "_fund_low"] = fundLow;
                    drFund[fundType + "_fund_moderate"] = fundMed;
                    drFund[fundType + "_fund_high"] = fundHigh;

                    drFund[fundType + "_alltime_fund_low"] = fundLowAllTime;
                    drFund[fundType + "_alltime_fund_moderate"] = fundMedAllTime;
                    drFund[fundType + "_alltime_fund_high"] = fundHighAllTime;

                    investmentFundRow.Low += fundLow.Value;
                    investmentFundRow.Med += fundMed.Value;
                    investmentFundRow.High += fundHigh.Value;

                    investmentFundRowAllTime.Low += fundLowAllTime.Value;
                    investmentFundRowAllTime.Med += fundMedAllTime.Value;
                    investmentFundRowAllTime.High += fundHighAllTime.Value;
                }

                listFundTotalInvestmentPerYear.Add(investmentFundRow);
                listFundTotalInvestmentPerYearAllTime.Add(investmentFundRowAllTime);

                dtFundBenefit.Rows.Add(drFund);
            }

            var errorMsgList = new List<string>();
            var display = true;
            
            if (product.NegativeYearValidation.HasValue)
            {
                var tahunKeInvestmentRow =
                    listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun >= 0 && (x.Tahun <= product.NegativeYearValidation.Value || x.Tahun <= dataInput.Premi.RencanaMasaPembayaran) && x.Med < 0);
                if (tahunKeInvestmentRow != null)
                {
                    display = false;
                    errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada tahun ke " + tahunKeInvestmentRow.Tahun);
                }
                
            }
            

            //if (product.NegativeAgeValidation.HasValue)
            //{
            //    var ageKeInvestmentRow =
            //        listFundTotalInvestmentPerYear.FirstOrDefault(x => x.Tahun > 10 && x.Tahun <= product.NegativeAgeValidation.Value - insAge && x.Low < 0);
            //    if (ageKeInvestmentRow != null && display)
            //        errorMsgList.Add("Nilai Estimasi Investasi Premi Berkala Anda bernilai negatif pada usia ke " + insAge);
            //}

            errorMsg = !errorMsgList.Any() ? null : errorMsgList.ToArray();

            return dtFundBenefit;
        }

        public static DataTable CalculateBenefitEducation(SummaryViewModel dataInput, List<ProductAllocationRate> productAllocationRates, List<CashValueRate> cashValueList, List<ProductTahapanRate> tahapanList, out string[] errorMsg)
        {
            errorMsg = null;

            var dtBenefit = new DataTable("Benefit");
            dtBenefit.Columns.AddRange(new[] {
                new DataColumn("Tahun"),
                new DataColumn("UsiaTertanggung"),
                new DataColumn("UsiaAnak"),
                new DataColumn("UangPertanggungan"),
                new DataColumn("PremiTahunan"),
                new DataColumn("DanaTahapan"),
                new DataColumn("AkumulasiDanaTahapan"),
                new DataColumn("Reserve"),
                new DataColumn("CashValue"),
                new DataColumn("DeathBenefit"),
                new DataColumn("Total")
            });

            var period = dataInput.Premi.MasaAsuransi;
            ProductAllocationRate prevAllocationRate = null;
            var stopGettingAllocationRate = false;
            decimal? akumulasiTahapanLastYear = 0;
            var insAge = 0;
            double interest = 6;
            decimal? premi = CalculatorServices.CurrencyToDecimal(dataInput.Premi.PremiBerkala) + CalculatorServices.CurrencyToDecimal(dataInput.Rider.BiayaAsuransiTambahan);
            decimal? up = CalculatorServices.CurrencyToDecimal(dataInput.Premi.UangPertanggungan);
            int payTerm = 0;
            int caraBayar = 1;
            decimal rsvTahunLalu = 0;

            if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus"))
            {
                payTerm = Convert.ToInt32(dataInput.Premi.RencanaMasaPembayaran);
            }
            else
            {
                caraBayar = Convert.ToInt32(dataInput.Premi.CaraBayar);
                payTerm = Convert.ToInt32(dataInput.Premi.PilihanMasaPembayaran);

                if (payTerm == 18)
                    payTerm = payTerm - dataInput.Nasabah.UmurAnak;
            }

            if (dataInput.Nasabah.TertanggungUtama == "Ya")
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurPemegangPolis);
            else
                insAge = Convert.ToInt32(dataInput.Nasabah.UmurTertanggungUtama);

            for (var i = insAge + 1; i <= (period + insAge); i++)
            {
                var tahunKe = i - insAge;
                var usiaAnak = dataInput.Nasabah.UmurAnak + tahunKe;
                decimal? lastYearRsv = 0;

                ProductAllocationRate allocationRate;
                ProductTahapanRate tahapanRate;
                CashValueRate cashValue;

                if (!stopGettingAllocationRate)
                {
                    allocationRate = productAllocationRates.FirstOrDefault(x => x.Year == tahunKe && x.ProductCode == dataInput.Nasabah.NamaProduk);
                    if (allocationRate == null)
                    {
                        allocationRate = prevAllocationRate;
                        stopGettingAllocationRate = true;
                    }
                    else
                    {
                        prevAllocationRate = allocationRate;
                    }
                }
                else
                {
                    allocationRate = prevAllocationRate;
                }

                var drFund = dtBenefit.NewRow();
                drFund["Tahun"] = tahunKe;
                drFund["UsiaTertanggung"] = i;
                drFund["UsiaAnak"] = usiaAnak;

                if (dataInput.Premi.ModeBayarPremi.Equals("sekaligus")) 
                {

                    drFund["PremiTahunan"] = tahunKe == 1 ? premi * caraBayar : null;
                } else
                {
                    if (dataInput.Premi.RencanaMasaPembayaran == 18)
                    {
                        drFund["PremiTahunan"] = usiaAnak <= dataInput.Premi.RencanaMasaPembayaran ? premi * caraBayar : null;
                    } else
                    {
                        drFund["PremiTahunan"] = tahunKe <= dataInput.Premi.RencanaMasaPembayaran ? premi * caraBayar : null;
                    }
                    
                }

                tahapanRate = tahapanList.FirstOrDefault(x => x.Year == usiaAnak && x.ProductCode == dataInput.Nasabah.NamaProduk);
                decimal? danaTahapan = 0;

                if (tahapanRate != null)
                    danaTahapan = usiaAnak == tahapanRate.Year ? (decimal)(tahapanRate.RateTahapan / 100) * up : 0;

                drFund["DanaTahapan"] = danaTahapan;

                var akumulasiTahapanTahunIni = akumulasiTahapanLastYear * (1 + ((decimal)interest / 100)) + danaTahapan;
                akumulasiTahapanLastYear = akumulasiTahapanTahunIni;
                drFund["AkumulasiDanaTahapan"] = akumulasiTahapanTahunIni;

                cashValue = cashValueList.FirstOrDefault(x => x.Year == tahunKe && x.InsuredAge == insAge && x.ChildAge == dataInput.Nasabah.UmurAnak && x.PaymentTerm == payTerm && x.Type == dataInput.Premi.ModeBayarPremi);
                decimal? cashValueTahunIni = 0;

                if (cashValue != null)
                    cashValueTahunIni = cashValue.CashValue * up / 10000000;

                drFund["CashValue"] = cashValueTahunIni;

                double factorX = 1 + (7.0 / 100);
                double factorY = 1 + (7.5 / 100);
                double factorZ = 40.0 / 100;

                if (tahunKe > 1)
                    lastYearRsv = Convert.ToDecimal(dtBenefit.Rows[dtBenefit.Rows.Count - 1]["Reserve"]);

                var rsvGuaranteed = cashValue != null ? cashValue.Reserve * (decimal)factorX : 0;
                var rsvAct = cashValue != null ? cashValue.Reserve * (decimal)factorY : 0;
                var delta = rsvAct - rsvGuaranteed;
                var rsv = delta * (decimal)factorZ;
                decimal reserveTahunIni = 0;
                decimal? fundReserve = 0;

                if (tahunKe > payTerm)
                {
                    reserveTahunIni = (decimal)lastYearRsv;
                    fundReserve = reserveTahunIni;
                }
                else
                {
                    reserveTahunIni = (decimal)rsvTahunLalu + (decimal)rsv;
                    fundReserve = reserveTahunIni * up / 10000000;
                }

                drFund["Reserve"] = fundReserve;
                rsvTahunLalu = (decimal)reserveTahunIni;
                var deathBenefit = dataInput.Premi.ModeBayarPremi == "berkala" ? (decimal)(allocationRate.RateDeathBenefit / 100) * up : up;
                drFund["DeathBenefit"] = deathBenefit;
                drFund["UangPertanggungan"] = deathBenefit;

                var total = deathBenefit + akumulasiTahapanTahunIni + fundReserve;
                drFund["Total"] = total;

                dtBenefit.Rows.Add(drFund);
            }

            return dtBenefit;
        }
    }
}
