﻿using Sales.Illustration.Web.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sales.Illustration.Web.Models
{
    public class ProductModel : Controller
    {
        //
        // GET: /ProductModel/

        private OnlineEntities on = new OnlineEntities();
        private OfflineEntities off = new OfflineEntities();
        private Sales.Illustration.Web.Helper.Generator gen = new Sales.Illustration.Web.Helper.Generator();

        public IEnumerable<Product> GetAllProduct()
        {
            if (_session.AppMode == "offline")
                return off.Products.AsNoTracking().ToList();
            else
                return on.Products.AsNoTracking().ToList();
        }

        public IEnumerable<Product> GetActiveProduct()
        {
            if (_session.AppMode == "offline")
                return off.Products.Where(x => x.IsActive == true).AsNoTracking().ToList();
            else
                return on.Products.Where(x => x.IsActive == true).AsNoTracking().ToList();
        }

        public Product GetProduct(string code)
        {
            if (_session.AppMode == "offline")
                return off.Products.Where(x => x.ProductCode == code).AsNoTracking().FirstOrDefault();
            else
                return on.Products.Where(x => x.ProductCode == code).AsNoTracking().FirstOrDefault();
        }

        public IEnumerable<Product> GetProductByType(string type)
        {
            if (_session.AppMode == "offline")
                return off.Products.Where(x => x.ProductType == type && x.IsActive == true).AsNoTracking().ToList();
            else
                return on.Products.Where(x => x.ProductType == type && x.IsActive == true).AsNoTracking().ToList();
        }

        public void AddProduct(Product data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Added;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Added;
                on.SaveChanges();
            }
        }

        public void EditProduct(Product data)
        {
            if (_session.AppMode == "offline")
            {
                off.Entry(data).State = EntityState.Modified;
                off.SaveChanges();
            }
            else
            {
                on.Entry(data).State = EntityState.Modified;
                on.SaveChanges();
            }
        }

    }
}
