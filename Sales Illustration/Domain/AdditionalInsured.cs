﻿using System;
using System.Collections.Generic;

namespace Sales.Illustration.Web.Domain
{
    public class AdditionalInsured
    {
        public string Nama { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public string RiskClass { get; set; }
        public decimal? BiayaAsuransi { get; set; }
        public string DateOfBirth { get; set; }
        public int? Age { get; set; }
        public string KelasPekerjaan { get; set; }
        public List<RiderItem> Riders { get; set; }
    }
}
