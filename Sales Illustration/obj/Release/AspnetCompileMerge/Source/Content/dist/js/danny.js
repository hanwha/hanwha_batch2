var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(document).ready(function () {
    //$('#progressbar-bawah li').eq(0).addClass("active");
    //$("#but-prev").attr("disabled", "disabled");
});

$(".selanjutnya").click(function () {

    var jumlah = $('#progressbar-bawah li').length;
    var posisi_aktif = $('#progressbar-bawah li.active').index();
    console.log(jumlah, posisi_aktif);
    if (posisi_aktif < jumlah) {
        $('#progressbar-bawah li').eq(posisi_aktif - 1).removeClass("active");
        $('#progressbar-bawah li').eq(posisi_aktif).addClass("active");
        document.getElementById("but-next").disabled = false;
        document.getElementById("but-prev").disabled = false;
        //$("button[name='selanjutnya']").removeAttr("disabled").button('refresh');
        //$("button[name='sebelumnya']").removeAttr("disabled").button('refresh');
    }

    if (posisi_aktif == (jumlah - 1)) {
        //$("button[name='selanjutnya']").attr("disabled", "disabled").button('refresh');
        document.getElementById("but-next").disabled = true;
    }


});

$(".sebelumnya").click(function () {
    var posisi_aktif = $('#progressbar li.active').index();
    console.log(posisi_aktif);
    if ((posisi_aktif + 1) >= 0) {

        $('#progressbar-bawah li').eq(posisi_aktif + 2).removeClass("active");
        $('#progressbar-bawah li').eq(posisi_aktif + 1).addClass("active");
        document.getElementById("but-next").disabled = false;
        document.getElementById("but-prev").disabled = false;

    }
    if ((posisi_aktif + 1) == 0) {
        document.getElementById("but-prev").disabled = true;
    }
});


var app = angular.module("myApp", []);
app.controller("myCtrl", function ($scope) {
    $scope.data = [
		{ 'nama': 'Danny Erry' },
		{ 'nama': 'Daniel Nugroho' },
		{ 'nama': 'Budi Nugroho' },
		{ 'nama': 'Susi Nugroho' },
		{ 'nama': 'Alamsyah Daniel' },
		{ 'nama': 'Toni Sentosa' },
		{ 'nama': 'Tatang Budiman' },
		{ 'nama': 'Teddy Sistiansyah' },
		{ 'nama': 'Bhima Aditya' },
		{ 'nama': 'Badrizka Hanif' },
		{ 'nama': 'Achmad Satrio' },
		{ 'nama': 'Dery Agistya' },
		{ 'nama': 'Shinta Yus' },
		{ 'nama': 'Bimo Tri' },
		{ 'nama': 'Hari Budi' },
		{ 'nama': 'sisworoso' },
		{ 'nama': 'Sri Panganti' },
    ];

    $scope.currentPage = 1;
    $scope.totalItems = $scope.data.length;
    $scope.numPerPage = 14;
    $scope.jumlah = Math.ceil($scope.totalItems / $scope.numPerPage);

    $scope.getNumber = function (num) {
        return new Array(num);
    }

    $scope.limitPage = function () {
        if ($scope.currentPage * $scope.numPerPage > $scope.data.length) {
            $scope.currentPage = 1;
        }
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.pageCount()) {
            $scope.currentPage++;
        }
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 1) {
            $scope.currentPage--;
        }
    };

    $scope.pageCount = function () {
        return Math.ceil($scope.totalItems / $scope.numPerPage);
    };


});

app.filter("paging", function () {
    return function (items, limit, currentPage) {
        if (typeof limit === 'string' || limit instanceof String) {
            limit = parseInt(limit);
        }

        var begin, end, index;
        begin = (currentPage - 1) * limit;
        end = begin + limit;
        var arrayToReturn = [];
        for (var i = 0; i < items.length; i++) {
            if (begin <= i && i < end) {
                arrayToReturn.push(items[i]);
            }
        }
        return arrayToReturn;
    };
});

$(function () {
    var xy = 0;
    $('.nav a').each(function (index) {
        if (this.href.trim().toLowerCase() == window.location.href.toLowerCase()) {
            $(this).addClass("active");
            xy += 1;
        }
    });
    if ($(".profile").hasClass("active")) {
        xy += 1;
    }
    if (xy == 0) {
        $(".profile").addClass("active");
    }
});

$(document).ready(function () {
    $('.dropdown').click(function (event) {
        if ($(".dropdown").hasClass("open")) {
            $('.dropdown').removeClass('open');
        } else {
            $('.dropdown').addClass('open');
        }


        if ($(".fa-caret-down").attr('style') == 'display: none;') {
            $(".fa-caret-down").removeAttr('style');
        } else {
            $(".fa-caret-down").css('display', 'none');
        }

        if ($(".fa-caret-right").attr('style') == 'display: none;') {
            $(".fa-caret-right").removeAttr('style');
        } else {
            $(".fa-caret-right").css('display', 'none');
        }

    });

    if ($(".profile").hasClass("active")) {
        $(".profile-active").attr("style", "display: inline");
        $(".profile-normal").attr("style", "display: none");
        $('.profile').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-profil-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-profil-active.png');
        });
    } else {
        $(".profile-active").attr("style", "display: none");
        $(".profile-normal").attr("style", "display: inline");
        $('.profile').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-profil-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-profil.png');
        });
    }

    if ($(".history").hasClass("active")) {
        $(".history-active").attr("style", "display: inline");
        $(".history-normal").attr("style", "display: none");
        $('.history').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-history-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-history-active.png');
        });

    } else {
        $(".history-active").attr("style", "display: none");
        $(".history-normal").attr("style", "display: inline");
        $('.history').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-history-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-history.png');
        });
    }

    if ($(".update").hasClass("active")) {
        $(".update-active").attr("style", "display: inline");
        $(".update-normal").attr("style", "display: none");
        $('.update').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-update-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-update-active.png');
        });

    } else {
        $(".update-active").attr("style", "display: none");
        $(".update-normal").attr("style", "display: inline");
        $('.update').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-update-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-update.png');
        });
    }

    if ($(".info").hasClass("active")) {
        $(".info-active").attr("style", "display: inline");
        $(".info-normal").attr("style", "display: none");
        $('.info').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-info-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-info-active.png');
        });

    } else {
        $(".info-active").attr("style", "display: none");
        $(".info-normal").attr("style", "display: inline");
        $('.info').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-info-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-info.png');
        });
    }

    if ($(".log").hasClass("active")) {
        $(".log-active").attr("style", "display: inline");
        $(".log-normal").attr("style", "display: none");
        $('.log').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-log-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-log-active.png');
        });

    } else {
        $(".log-active").attr("style", "display: none");
        $(".log-normal").attr("style", "display: inline");
        $('.log').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-log-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-log.png');
        });
    }

    if ($(".exit").hasClass("active")) {
        $(".exit-active").attr("style", "display: inline");
        $(".exit-normal").attr("style", "display: none");
        $('.exit').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/exit.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/exit-grey.png');
        });

    } else {
        $(".exit-active").attr("style", "display: none");
        $(".exit-normal").attr("style", "display: inline");
        $('.exit').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/exit.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/exit-grey.png');
        });
    }

    if ($(".dropdown-anak").hasClass("active")) {
        $(".dropdown-toggle").addClass("active");
        $(".dropdown-toggle").attr("style", "color: #ff880c;");
        $(".dropdown").addClass("open");
    } else {
        $(".dropdown-toggle").removeClass("active");
    }

    if ($(".dropdown-toggle").hasClass("active")) {
        $(".produk-active").attr("style", "display: inline");
        $(".produk-normal").attr("style", "display: none");
        $('.dropdown-toggle').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-product-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-product-active.png');
        });
    } else {
        $(".produk-active").attr("style", "display: none");
        $(".produk-normal").attr("style", "display: inline");
        $('.dropdown-toggle').hover(function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-product-active.png');
        }, function () {
            $(this).find('img').attr('src', '/Content/dist/img/icon-product.png');
        });
    }

});






