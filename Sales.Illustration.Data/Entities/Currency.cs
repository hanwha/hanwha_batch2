﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class Currency
    {
        public Currency()
        {
            this.ProductCurrencies = new HashSet<ProductCurrency>();
        }

        [Key]
        public string CurrCode { get; set; }
        public string CurrName { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual ICollection<ProductCurrency> ProductCurrencies { get; set; }
    }
}
