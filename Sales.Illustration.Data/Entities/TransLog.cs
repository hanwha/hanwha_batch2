﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class TransLog
    {
        public TransLog()
        {
            this.AdditionalInsuredDatas = new HashSet<AdditionalInsuredData>();
            this.InvestmentDatas = new HashSet<InvestmentData>();
            this.NasabahDatas = new HashSet<NasabahData>();
            this.PremiumDatas = new HashSet<PremiumData>();
            this.RiderDatas = new HashSet<RiderData>();
            this.SummaryDatas = new HashSet<SummaryData>();
            this.TopUpWithdrawalDatas = new HashSet<TopUpWithdrawalData>();
        }

        [Key]
        public string TransCode { get; set; }
        public string AgentCode { get; set; }
        public Nullable<int> TransStatus { get; set; }
        public Nullable<System.DateTime> TransDate { get; set; }

        public virtual ICollection<AdditionalInsuredData> AdditionalInsuredDatas { get; set; }
        public virtual ICollection<InvestmentData> InvestmentDatas { get; set; }
        public virtual ICollection<NasabahData> NasabahDatas { get; set; }
        public virtual ICollection<PremiumData> PremiumDatas { get; set; }
        public virtual ICollection<RiderData> RiderDatas { get; set; }
        public virtual ICollection<SummaryData> SummaryDatas { get; set; }
        public virtual ICollection<TopUpWithdrawalData> TopUpWithdrawalDatas { get; set; }
        public virtual UserMembership UserMembership { get; set; }
    }
}
