﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class PremiumData
    {
        [Key]
        public long PremiumDataId { get; set; }
        public string TransCode { get; set; }
        public string CurrCode { get; set; }
        public Nullable<int> PaymentPeriod { get; set; }
        public string PMCode { get; set; }
        public Nullable<decimal> RegularPremium { get; set; }
        public Nullable<decimal> RegularTopUp { get; set; }
        public Nullable<decimal> SumInsured { get; set; }

        public virtual TransLog TransLog { get; set; }
    }
}
