﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class RiskClass
    {
        [Key]
        public int RiskClassId { get; set; }
        public string RiskClassDesc { get; set; }
        public Nullable<int> Class { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
