﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Illustration.Data.Entities
{
    public partial class RiderType
    {
        public RiderType()
        {
            this.RiderRates = new HashSet<RiderRate>();
        }

        [Key]
        public int RiderTypeId { get; set; }
        public int RiderId { get; set; }
        public string TypeName { get; set; }
        public Nullable<int> MinAge { get; set; }
        public Nullable<int> MaxAge { get; set; }
        public Nullable<int> CoverTerm { get; set; }
        public Nullable<int> ParentRiderType { get; set; }
        public Nullable<bool> IsActive { get; set; }

        public virtual Rider Rider { get; set; }
        public virtual ICollection<RiderRate> RiderRates { get; set; }
    }
}
